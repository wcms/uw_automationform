/**
 * @file
 */

(function ($) {
  $(document).ready(function () {
    // Update fields when a new pool is chosen.
    $("#edit-uw-automate-pool").change(function () {
      server = $(this).val();
      url = 'https://' + Drupal.settings.uw_automationform.pool_info[server].host + '/';
      $("#edit-uw-automate-url").val(url);
    });
  });
}(jQuery));
